import typing
import itertools
import logging

import PySide2
from PySide2.QtWidgets import QHeaderView, QLineEdit
from PySide2.QtCore import Signal, Qt


# Based on
# https://stackoverflow.com/a/44346317/774130
# https://stackoverflow.com/a/44476825/774130

# PyQt vs PySide differences in signal handling
# https://www.learnpyqt.com/blog/pyqt5-vs-pyside2/

class LogTableFilterHeader(QHeaderView):
    filterExpressionChanged = Signal()

    def __init__(self, parent: typing.Optional[PySide2.QtWidgets.QWidget]):
        super().__init__(
            Qt.Horizontal,
            parent)

        self._do_not_handle_filter_expression_changes = False

        self._editors = []
        self._padding = 2
        self.setStretchLastSection(True)
        self.setDefaultAlignment(
            Qt.AlignLeft | \
            Qt.AlignVCenter)

        self.setSortIndicatorShown(True)
        self.setSectionsClickable(True)
        self.sectionResized.connect(self.adjustPositions)
        parent.horizontalScrollBar().valueChanged.connect(
            self.adjustPositions)


    def setFilterBoxes(self, count, placeholders: typing.Optional[typing.List[str]] = None):
        while self._editors:
            editor = self._editors.pop()
            editor.deleteLater()

        if placeholders is None:
            placeholders = []

        for _, placeholder in itertools.zip_longest(range(count), placeholders):
            editor = QLineEdit(self.parent())
            editor.setPlaceholderText(placeholder if placeholder else 'Filter')
            editor.returnPressed.connect(self.handleFilterExpressionChange)
            self._editors.append(editor)
        self.adjustPositions()

    def sizeHint(self):
        size = super().sizeHint()
        if self._editors:
            height = self._editors[0].sizeHint().height()
            size.setHeight(size.height() + height + self._padding)
        return size

    def updateGeometries(self):
        if self._editors:
            height = self._editors[0].sizeHint().height()

            # below header
            #self.setViewportMargins(0, 0, 0, height + self._padding)

            # above header
            self.setViewportMargins(0, height + self._padding, 0, 0)
        else:
            self.setViewportMargins(0, 0, 0, 0)
        super().updateGeometries()
        self.adjustPositions()

    def adjustPositions(self):
        for index, editor in enumerate(self._editors):
            height = editor.sizeHint().height()
            editor.move(
                self.sectionPosition(index) - self.offset() + 2,
                0)
                #height + (self._padding // 2))
            editor.resize(self.sectionSize(index), height)

    def filterText(self, index):
        if 0 <= index < len(self._editors):
            return self._editors[index].text()
        return ''

    def setFilterText(self, index, text):
        if 0 <= index < len(self._editors):
            self._editors[index].setText(text)

    def clearFilters(self):
        logging.debug("LogTableFilterHeader.clearFilters()")
        self._do_not_handle_filter_expression_changes = True
        try:
            for editor in self._editors:
                editor.clear()
        finally:
            self._do_not_handle_filter_expression_changes = False

        self.filterExpressionChanged.emit()

    def handleFilterExpressionChange(self):
        if not self._do_not_handle_filter_expression_changes:
            logging.debug("LogTableFilterHeader.handleFilterExpressionChange()")
            self._do_not_handle_filter_expression_changes = True
            try:
                self.filterExpressionChanged.emit()
            finally:
                self._do_not_handle_filter_expression_changes = False
