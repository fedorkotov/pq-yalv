import typing, logging

from PySide2 import QtCore, QtGui
from PySide2.QtCore import Qt
import pandas as pd

from log4net_reader import Log4NetDataLoader
from loglevel_filters import Log4NetLogLevel

# Based on
# https://www.learnpyqt.com/courses/model-views/qtableview-modelviews-numpy-pandas/

class LogTableModel(QtCore.QAbstractTableModel):
    DEFAULT_COLOR = QtGui.QColor('fuchsia')

    def __init__(self, log_levels: typing.List[Log4NetLogLevel]):
        super(LogTableModel, self).__init__()
        
        self._data = None

        self._loglevel_colors = \
            {lvl.name: lvl.color for lvl in log_levels}

        self.replace_log_gata(
            Log4NetDataLoader.get_empty_data())        
        
    def replace_log_gata(self, data: pd.DataFrame):
        self.beginResetModel()
        self._data = data
        self._init_special_column_indexes()    
        self.endResetModel()

    def _init_special_column_indexes(self):
        self._timestamp_idx = \
            self._data\
                .columns\
                .get_loc(Log4NetDataLoader.TIMESTAMP_COLNAME)

        self._loglevel_idx = \
            self._data\
                .columns\
                .get_loc(Log4NetDataLoader.LEVEL_COLUMN_NAME)

        self._message_idx = \
            self._data\
                .columns\
                .get_loc(Log4NetDataLoader.MESSAGE_COLUMN_NAME)

        self._throwable_idx = \
            self._data\
                .columns\
                .get_loc(Log4NetDataLoader.THROWABLE_COLUMN_NAME)
    
    @property
    def log_level_column_idx(self) -> int:
        return self._loglevel_idx+1

    def throwable_column_idx(self) -> int:
        return self._throwable_idx+1

    def _get_row_bgcolor(self, row_idx: int) -> QtGui.QColor:
        level = self._data.iloc[row_idx, self._loglevel_idx]
        return self._loglevel_colors[level] \
               if level in self._loglevel_colors \
               else LogTableModel.DEFAULT_COLOR

    def getMessage(self, row_idx: int) -> str:
        if row_idx < 0:
            return "<!Row not selected>"
        elif row_idx > self._data.shape[0]:
            return "<!Index %d out of range>" % row_idx
        else:
            return self._data.iloc[row_idx, self._message_idx]

    def getThrowable(self, row_idx: int) -> str:
        if row_idx < 0:
            return "<!Row not selected>"
        elif row_idx > self._data.shape[0]:
            return "<!Index %d out of range>" % row_idx
        else:
            return self._data.iloc[row_idx, self._throwable_idx]

    def data(self, index, role):
        if role == Qt.DisplayRole:
            idx_column = index.column()
            if idx_column == 0:
                return self._data.index[index.row()]
            else:
                # Row index is displayed in first column
                # Index is not an ordinary column in pandas datasource.
                # Subtracting 1 to convert column index to pandas column
                idx_column -= 1
                if idx_column == self._timestamp_idx:
                    return self._data.iloc[index.row(), idx_column].isoformat(' ')
                else:
                    return self._data.iloc[index.row(), idx_column]
        elif role == Qt.BackgroundRole:
            return self._get_row_bgcolor(index.row())

    def rowCount(self, index):
        return self._data.shape[0]

    def columnCount(self, index):
        return self._data.shape[1]+1

    def headerData(self, section, orientation, role):
        # section is the index of the row.
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section == 0:
                    return "idx"
                else:
                    return self._data.columns[section-1]