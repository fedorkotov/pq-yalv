from PySide2.QtCore import QRect, Slot, QMargins
from PySide2.QtGui import QPainter
from PySide2.QtWidgets import QHeaderView, QStyleOptionButton, QStyle

# Based on
# https://stackoverflow.com/a/9767888
# https://stackoverflow.com/a/34175552

class LogTableCheckboxHeaderView(QHeaderView):
    def __init__(self, orientation, parent=None):
        super().__init__(orientation, parent)
        self.checkboxesList = []
        self.sectionCountChanged.connect(self.onSectionCountChanged)
        margins = QMargins(35,0,0,0)
        super().setContentsMargins(margins)

    def paintSection(self, painter: QPainter, rect: QRect, logicalIndex: int):
        painter.save()
        super().paintSection(painter, rect, logicalIndex)
        painter.restore()


        painter.save()
        painter.translate(rect.topLeft())

        option = QStyleOptionButton()
        option.rect = QRect(5, 5, 10, 10)
        if (len(self.checkboxesList) != self.count()):
            self.onSectionCountChanged(len(self.checkboxesList), self.count())

        if self.checkboxesList[logicalIndex]:
            option.state = QStyle.State_On
        else:
            option.state = QStyle.State_Off
        self.style().drawControl(QStyle.CE_CheckBox, option, painter)
        painter.restore()

    def mousePressEvent(self, event):
        iIdx = self.logicalIndexAt(event.pos())
        self.checkboxesList[iIdx] = not self.checkboxesList[iIdx]
        self.updateSection(iIdx)
        super().mousePressEvent(event)

    @Slot()
    def onSectionCountChanged(self, oldCount,  newCount):
        if newCount > oldCount:
            for _ in range(newCount - oldCount):
                self.checkboxesList.append(False)
        else:
            self.checkboxesList = self.checkboxesList[0:newCount]

