import re
import typing

from PySide2.QtCore import Qt, QSortFilterProxyModel

# Based on
# https://stackoverflow.com/a/57845903

class LogSortFilterProxyModel(QSortFilterProxyModel):
    def __init__(self, *args, **kwargs):
        QSortFilterProxyModel.__init__(self, *args, **kwargs)
        self.__regular_expressions = {}
        self.__allowed_values = {}
        self.__columns_with_conditions = set()

    @staticmethod
    def _convertFilterRegex(regex: typing.Union[typing.Pattern, str]) -> typing.Pattern:
        if isinstance(regex, str):
            return re.compile(regex)
        else:
            return regex

    def __updateColumnsWithFilters(self):
        # Recalculating set on each filter update is ineffective
        # (as opposed to removing/adding only indexes of columns with changes).
        # But log table contains at most 10-15 columns
        # and conditions are not updated that often.
        # So this ineffectiveness does not matter.
        self.__columns_with_conditions = set()
        self.__columns_with_conditions\
            .update(self.__regular_expressions.keys())
        self.__columns_with_conditions\
            .update(self.__allowed_values.keys())


    def setAllowedValuesForColumn(
        self,
        column_idx: int,
        allowed_values: typing.Optional[typing.List[str]]):

        self.__allowed_values[column_idx] = set(allowed_values)
        self.__updateColumnsWithFilters()
        self.invalidateFilter()

    def setRegexFilterByColumn(
        self,
        column_idx: int,
        regex: typing.Union[typing.Pattern, str]):

        self.__regular_expressions[column_idx] = \
            LogSortFilterProxyModel._convertFilterRegex(regex)
        self.__updateColumnsWithFilters()
        self.invalidateFilter()

    def setRegexFilters(
        self, 
        filters: typing.Dict[int, typing.Union[typing.Pattern, str]]):
        
        self.__regular_expressions = \
            {col_idx:LogSortFilterProxyModel._convertFilterRegex(regex) \
             for col_idx, regex in filters.items()\
             if regex}
        self.__updateColumnsWithFilters()
        self.invalidateFilter()

    def clearRegexFilterForColumn(self, column):
        del self.__regular_expressions[column]
        self.__updateColumnsWithFilters()
        self.invalidateFilter()

    def clearAllowedValuesForColumn(self, column):
        del self.__allowed_values[column]
        self.__updateColumnsWithFilters()
        self.invalidateFilter()

    def clearRegexFilters(self):
        self.__regular_expressions.clear()
        self.__updateColumnsWithFilters()
        self.invalidateFilter()

    def clearAllowedValues(self):
        self.__allowed_values.clear()
        self.__updateColumnsWithFilters()
        self.invalidateFilter()

    def filterAcceptsRow(self, source_row, source_parent):
        if not self.__columns_with_conditions:
            return True

        for col_idx in self.__columns_with_conditions:
            index = self.sourceModel().index(source_row, col_idx, source_parent)
            text = self.sourceModel().data(index, Qt.DisplayRole)
            if text is None:
                text = ''

            allowed_values = self.__allowed_values.get(col_idx, None)
            if not (allowed_values is None) \
                and (not text in allowed_values):

                return False

            regex = self.__regular_expressions.get(col_idx, None)
            if regex and (not bool(regex.match(text))):
                return False
        return True

    def lessThan(self, left, right):
        leftData = self.sourceModel().data(left, Qt.DisplayRole)
        rightData = self.sourceModel().data(right, Qt.DisplayRole)
        return leftData < rightData
