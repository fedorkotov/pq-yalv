import unittest
import io

import yaml

from .yaml_file_io import YAMLConfigFileIO
from .yaml_stream_io import YAMLConfigStreamIO
from .config_codec import PQYALVConfigCodec
from files_selector import FavouriteDirectory

class TestPQYALVConfig(unittest.TestCase):

    def test_default_empty_favourites(self):
        cfg_stream = io.StringIO()
        cfg_stream_io = YAMLConfigStreamIO(cfg_stream)
        cfg_codec = PQYALVConfigCodec(cfg_stream_io)
        self.assertEqual(
            len(cfg_codec.get_favourites()),
            0)

    def test_read_favourites(self):
        cfg_stream = io.StringIO(
            """
            favourites:
                - name: applogs1
                  path: c:\\Program Files\\App1\\logs
                - name: Application logs2
                  path: /var/log/app2/
            """)

        cfg_stream_io = YAMLConfigStreamIO(cfg_stream)
        cfg_codec = PQYALVConfigCodec(cfg_stream_io)

        cfg_codec.read_config()
        favourites = cfg_codec.get_favourites()

        self.assertEqual(
            len(favourites),
            2)

        self.assertEqual(
            favourites[0].name,
            "applogs1")
        self.assertEqual(
            favourites[0].path,
            "c:\\Program Files\\App1\\logs")

        self.assertEqual(
            favourites[1].name,
            "Application logs2")
        self.assertEqual(
            favourites[1].path,
            "/var/log/app2/")

    def test_write_empty_favourites(self):
        cfg_stream = io.StringIO()
        cfg_stream_io = YAMLConfigStreamIO(cfg_stream)
        cfg_codec = PQYALVConfigCodec(cfg_stream_io)
        
        cfg_codec.write_config()
        
        cfg_stream.seek(0)        
        cfg_yaml = yaml.safe_load(cfg_stream)
        self.assertTrue('favourites' in cfg_yaml)
        self.assertEquals(len(cfg_yaml['favourites']), 0)

    def test_write_favourites(self):
        cfg_stream = io.StringIO()
        cfg_stream_io = YAMLConfigStreamIO(cfg_stream)
        cfg_codec = PQYALVConfigCodec(cfg_stream_io)

        favourites = \
            [FavouriteDirectory(
                "applogs1",
                "c:\\Program Files\\App1\\logs"),
            FavouriteDirectory(
                "Application logs2",
                "/var/log/app2/")]

        cfg_codec.set_favourites(favourites)
        cfg_codec.write_config()                
        
        cfg_stream.seek(0)
        cfg_yaml = yaml.safe_load(cfg_stream)
        self.assertTrue('favourites' in cfg_yaml)
        favourites_yaml = cfg_yaml['favourites']

        self.assertEquals(len(favourites_yaml), 2)

        self.assertEqual(
            favourites_yaml[0]['name'],
            "applogs1")
        self.assertEqual(
            favourites_yaml[0]['path'],
            "c:\\Program Files\\App1\\logs")

        self.assertEqual(
            favourites_yaml[1]['name'],
            "Application logs2")
        self.assertEqual(
            favourites_yaml[1]['path'],
            "/var/log/app2/")
                
if __name__ == '__main__':
    unittest.main()