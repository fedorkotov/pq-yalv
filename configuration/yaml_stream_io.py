import yaml

class YAMLConfigStreamIO:
    """
    Reads and writes config data to and from
    given stream in YAML format.
    Does not care about actual config contents.
    """
    def __init__(self, config_stream):
        self.__config_stream = config_stream

    def read_config(self):
        return yaml.safe_load(self.__config_stream)

    def write_config(self, config_data):
        yaml.safe_dump(
            config_data,
            self.__config_stream)