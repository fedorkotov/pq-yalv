
import os
import logging

from .yaml_stream_io import YAMLConfigStreamIO

class YAMLConfigFileIO:
    """
    Reads and writes config data to and from file with
    specified path in YAML format.
    Does not care about actual config contents.
    """

    def __init__(self, config_path, defaults_factory = None):
        self.__defaults_factory = defaults_factory
        self.__config_path = config_path

    def read_config(self):
        if not os.path.isfile(self.__config_path):
            logging.debug(
                "Config file '%s' does not exits. Assuming defaults",
                self.__config_path)
            if self.__defaults_factory:
                return self.__defaults_factory()
            else:
                return {}

        with open(self.__config_path, encoding="utf-8") as f:
            return YAMLConfigStreamIO(f).read_config()

    def write_config(self, config_data):
        with open(self.__config_path, mode='w', encoding="utf-8") as f:
            YAMLConfigStreamIO(f).write_config(config_data)