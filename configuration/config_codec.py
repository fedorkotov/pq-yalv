
import logging
from typing import List

from files_selector import FavouriteDirectory
from common import is_pathname_valid

FAVOURITES_KEY = 'favourites'

class PQYALVConfigCodec:
    """
    Transforms config data to and from concrete
    classes to dictionaries and lists
    that YAMLConfigIO or some other
    IO implementation can work with
    """
    def __init__(self, config_io):
        self.__config_io = config_io

        # raw config contents are preserved
        # to avoid overwritting parameters that
        # current version of program does not know about
        self.__raw_config = {}
        self.__favourites = None
        self.__set_defaults()

    def __set_defaults(self):
        self.__favourites = []

    def read_config(self):
        # saving to tmp variables and overwritting
        # instance properties only after
        # successfull reading and conversion.
        raw_config = self.__config_io.read_config()
        favourites = self._decode_favourites(raw_config)

        self.__raw_config = raw_config
        self.__favourites = favourites

    def write_config(self):
        self._encode_favourites()
        self.__config_io.write_config(self.__raw_config)

    def set_favourites(self, favourites: List[FavouriteDirectory]):
        self.__favourites = favourites

    def get_favourites(self) -> List[FavouriteDirectory]:
        return self.__favourites

    def _decode_favourites(self, raw_config):
        raw_favourites = \
            raw_config\
            .get(FAVOURITES_KEY,[])

        favourites = []
        for f in raw_favourites:
            name = f.get('name', '<unknown>')
            path = f.get('path')
            if not path:
                logging.error(
                    "Favourites item '%s' in config does not contain "
                    "folder path and will be ignored")
                continue
            if not is_pathname_valid(path):
                logging.warning(
                    "Favourites path '%s' in config is not a valid path")
            favourites.append(
                FavouriteDirectory(name, path))

        return favourites

    def _encode_favourites(self):
        self.__raw_config[FAVOURITES_KEY] = \
            [{'name': f.name,
              'path': f.path}\
             for f in self.__favourites]
