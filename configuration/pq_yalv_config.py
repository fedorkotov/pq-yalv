import os
import logging

from appdirs import user_config_dir

from .yaml_file_io import YAMLConfigFileIO
from .config_codec import PQYALVConfigCodec

def configure_config_codec(app_name):
    """
    Configures PQYALVConfigCodec and YAMLConfigFileIO
    to use config in OS-specific localtion inside user
    home folder
    """
    config_dir = user_config_dir(app_name)
    if not os.path.isdir(config_dir):
        logging.debug("Creating config directory %s", config_dir)
        os.makedirs(config_dir)

    config_path = \
        os.path.join(
            user_config_dir(app_name),
            app_name+".yml")
    logging.debug("Config path is %s", config_path)

    config_io = YAMLConfigFileIO(config_path)
    return PQYALVConfigCodec(config_io)
