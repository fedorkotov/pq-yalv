import re 

# taken from here https://stackoverflow.com/a/2669120
# Author: Mark Byers https://stackoverflow.com/users/61974/mark-byers
# based on https://blog.codinghorror.com/sorting-for-humans-natural-sort-order/
def sorted_alphanumerically(l): 
    """ Sort the given iterable in the way that humans expect.""" 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)