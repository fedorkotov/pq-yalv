import unittest

from .alphanumeric_sort import sorted_alphanumerically

class TestAlphanumericSort(unittest.TestCase):
    def test_empty_list(self):
        self.assertEqual(
            len(sorted_alphanumerically([])),
            0)

    def test_same_filename_base(self):
        input_list = [
            "log11.xml",
            "log22.xml",
            "log033.xml",
            "log1.xml",            
            "log2.xml"]
        
        output_list = sorted_alphanumerically(input_list)

        self.assertSequenceEqual(
            [
            "log1.xml",
            "log2.xml",
            "log11.xml",
            "log22.xml",
            "log033.xml"],
            output_list)

    def test_different_filename_base(self):
        input_list = [
            "a-log11.xml",
            "b-log22.xml",
            "a-log033.xml",
            "b-log1.xml",            
            "b-log2.xml"]
        
        output_list = sorted_alphanumerically(input_list)

        self.assertSequenceEqual(
            [
            "a-log11.xml",
            "a-log033.xml",
            "b-log1.xml",
            "b-log2.xml",
            "b-log22.xml"],
            output_list)

    def test_date_in_name(self):
        input_list = [
            "log.2020-09-20.11.xml",
            "log.2020-09-20.2.xml",
            "log.2020-09-20.1.xml",
            "log.2020-09-19.11.xml",
            "log.2020-09-19.1.xml",
            "log.2020-09-19.2.xml",]
        
        output_list = sorted_alphanumerically(input_list)

        self.assertSequenceEqual(
            [
            "log.2020-09-19.1.xml",
            "log.2020-09-19.2.xml",
            "log.2020-09-19.11.xml",
            "log.2020-09-20.1.xml",
            "log.2020-09-20.2.xml",
            "log.2020-09-20.11.xml"],
            output_list)