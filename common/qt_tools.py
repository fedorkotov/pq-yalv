from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QToolButton, QGroupBox
from PySide2.QtCore import Qt, QSize
from PySide2.QtGui import QIcon

MAIN_PANEL_BUTTON_SIZE = 64
MAIN_PANEL_FONT_SIZE = 6
MAIN_PANEL_ICON_SIZE = 48   

GROUP_BOX_STYLE =\
    """QGroupBox {
            border: 1px solid silver;
            border-radius: 6px;
            margin-top: 10px;
        }

        QGroupBox::title {
            subcontrol-origin: margin;
            left: 7px;
            padding: 0px 3px 0px 3px;
        }"""

def create_tool_panel_button(
    caption: str, 
    icon: QIcon,
    button_size: int = MAIN_PANEL_BUTTON_SIZE,
    font_size: int = MAIN_PANEL_FONT_SIZE,
    icon_size: int = MAIN_PANEL_ICON_SIZE):

    button = QToolButton()

    button.setFixedSize(
        button_size,
        button_size)

    button.setText(caption)
    button.setToolButtonStyle(\
        Qt.ToolButtonStyle.ToolButtonTextUnderIcon)

    button.setIcon(icon)
    button.setIconSize(\
        QSize(icon_size,\
              icon_size))

    # https://stackoverflow.com/questions/13984811/how-to-set-the-font-size-of-the-label-on-pushbutton-in-qt
    btnFont = button.font()
    btnFont.setPointSize(font_size)
    button.setFont(btnFont)

    return button


def create_group_box(caption: str):
    # By default group box title is shown above frame on
    # my system (Ubuntu 18.04, GNOME Shell).
    # In original YALV it overlaps frame.
    # Using styles seems to be the only way to adjust
    # title position
    # https://stackoverflow.com/a/42668561/774130
    groupBox = QGroupBox(caption)
    groupBox.setStyleSheet(GROUP_BOX_STYLE)
    return groupBox    