#!/usr/bin/env python3
import sys
import os
import typing
import logging
import logging.handlers
import platform
import copy

from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QVBoxLayout, QHBoxLayout, QPlainTextEdit, \
                              QWidget, QSplitter, QLabel, \
                              QGroupBox, QGridLayout, QToolButton, \
                              QListWidget, QListWidgetItem, QFileDialog, \
                              QDialog, QAbstractItemView
from PySide2.QtCore import Qt, QItemSelection, QMargins, QSize
from PySide2.QtGui import QColor, QIcon
import pandas as pd

from appdirs import user_log_dir

from log4net_reader import Log4NetDataLoader

from log_table import \
    LogTableModel, LogSortFilterProxyModel, \
    LogTableFilterHeader

from loglevel_filters import \
    Log4NetLogLevel, LogLevelFilterCheckBoxes, \
    LogLevelFilterRadioButtons

from files_selector import FavouriteDirectory, LogFilesSelector, \
                           FavouriteDirectoriesListEditor

from common import create_tool_panel_button,\
                   create_group_box

from configuration import configure_config_codec                  

# Based on
# https://www.learnpyqt.com/courses/model-views/qtableview-modelviews-numpy-pandas/

APP_NAME = 'pq-YALV'
LOG_DIR = user_log_dir(APP_NAME)
LOG_PATH = \
    os.path.join(
        LOG_DIR,
        APP_NAME + ".log")

# Use NSURL as a workaround to pyside/Qt4 behavior for dragging and dropping on OSx
# https://gist.github.com/benjaminirving/f45de3bbabbcacd3ca29
op_sys = platform.system()
if op_sys == 'Darwin':
    from Foundation import NSURL

class MainWindow(QtWidgets.QMainWindow):
    LOG_LEVELS = [\
        Log4NetLogLevel('TRACE', QColor('lightgray')),
        Log4NetLogLevel('DEBUG', QColor('azure')),
        Log4NetLogLevel('INFO',  QColor('lightgreen')),
        Log4NetLogLevel('WARN',  QColor('yellow')),
        Log4NetLogLevel('ERROR', QColor('lightsalmon')),
        Log4NetLogLevel('FATAL', QColor('red'))]

    @staticmethod
    def _load_from_files(file_paths: [str]) -> pd.DataFrame:
        data_loader = Log4NetDataLoader()
        data_loader.append_files(file_paths)
        return data_loader.get_dataframe()

    @staticmethod
    def _setup_table_models() -> typing.Tuple[LogTableModel, LogSortFilterProxyModel]:
        model = LogTableModel(MainWindow.LOG_LEVELS)
        proxy = LogSortFilterProxyModel()
        proxy.setSourceModel(model)
        return model, proxy

    @staticmethod
    def _setup_filter_header(
        model: QtCore.QAbstractItemModel, 
        parent: typing.Optional[QtWidgets.QWidget]) -> LogTableFilterHeader:
        
        filter_header = LogTableFilterHeader(parent)
        column_count = model.columnCount()
        filter_header.setFilterBoxes(
            column_count,
            placeholders=[\
                "filter {}".format(
                    model.headerData(
                        idx,
                        Qt.Horizontal,
                        Qt.DisplayRole)\
                    .lower()) \
                for idx in range(column_count)])
        return filter_header
    
    def __init__(self):
        super().__init__()

        self.setWindowTitle('pq-YALV')
        self.setWindowIcon(QIcon('resources/icons/app.ico'))

        self.__config = configure_config_codec(APP_NAME)
        self.__config.read_config()

        self._initialize_log_table()
        self._initialize_message_and_exception_viewers()
        self._initialize_tool_panel_widgets()
        self._initialize_files_list()

        self._layout_message_and_exception_viewers()
        self._layout_log_table_and_msg_exception_viewers()
        self._layout_tool_panel()
        self._do_not_handle_filter_changes = False

        self.setCentralWidget(self._rootWidget)
            
        self.logFilesSelector\
            .setFavouriteDirectories(
                self.__config.get_favourites())

        self.logTable.setColumnHidden(
            self.model.throwable_column_idx(),
            True)

        # Selection is not initialized by now.
        # When window is first shown, the first row is selected
        # but code below does not set message and error.
        #
        # self._updateMessageErrorViewers(
        #     self.logTable\
        #         .selectionModel()\
        #         .selection())

        self._setMessageAndErrorFromRow(0)

        self.setAcceptDrops(True)

    #----------------------------------------------------------------------
    # Components initialization and widgets placement
    #----------------------------------------------------------------------

    def _initialize_log_table(self):
        self.logTable = QtWidgets.QTableView()

        self.model, self.sortFilterProxyModel = \
            MainWindow._setup_table_models()
        self.logTable.setModel(self.sortFilterProxyModel)

        self.filterHeader = \
            MainWindow._setup_filter_header(
                self.sortFilterProxyModel,
                self.logTable)
        self.logTable.setHorizontalHeader(self.filterHeader)
        self.logTable.setSortingEnabled(True)
        self.filterHeader\
            .filterExpressionChanged\
            .connect(self.handleFilterExpressionChanged)

        self.logTable.setSelectionBehavior(QAbstractItemView.SelectRows)

    def _initialize_message_and_exception_viewers(self):
        self.messageViewer = QPlainTextEdit()
        self.messageViewer.setReadOnly(True)

        self.exceptionViewer = QPlainTextEdit()
        self.exceptionViewer.setReadOnly(True)

        selectionModel = self.logTable.selectionModel()
        selectionModel.selectionChanged.connect(self.handleLogTableSelectionChanged)

    @staticmethod
    def _layout_with_label_on_top(widget: QWidget, label: str) -> QWidget:
        layout = QVBoxLayout()
        layout.addWidget(QLabel(label))
        layout.addWidget(widget)

        widgetWithLabel = QWidget()
        widgetWithLabel.setLayout(layout)

        return widgetWithLabel

    def _layout_message_and_exception_viewers(self):
        messageViewerWithLabel = \
            MainWindow._layout_with_label_on_top(
                self.messageViewer,
                "Message:")

        exceptionViewerWithLabel = \
            MainWindow._layout_with_label_on_top(
                self.exceptionViewer,
                "Throwable:")

        self._messageExceptionSplitter = QSplitter()
        self._messageExceptionSplitter.setOrientation(Qt.Horizontal)
        self._messageExceptionSplitter.addWidget(messageViewerWithLabel)
        self._messageExceptionSplitter.addWidget(exceptionViewerWithLabel)


    def _layout_log_table_and_msg_exception_viewers(self):
        self._logTableMessageExceptionSplitter = QSplitter()
        self._logTableMessageExceptionSplitter.setOrientation(Qt.Vertical)
        self._logTableMessageExceptionSplitter.addWidget(self.logTable)
        self._logTableMessageExceptionSplitter.addWidget(self._messageExceptionSplitter)

    def _initialize_tool_panel_widgets(self):
        self.levelFilterCheckBoxes = LogLevelFilterCheckBoxes(self.LOG_LEVELS)
        self.levelFilterCheckBoxes\
            .selectedLevelsChanged\
            .connect(self.handleLogLevelCheckBoxesFilterChanged)

        self.levelFilterRadioButtons = LogLevelFilterRadioButtons(self.LOG_LEVELS)
        self.levelFilterRadioButtons\
            .selectedLevelsChanged\
            .connect(self.handleLogLevelRadioButtonFilterChanged)

        # If Primo icon set can not be used for some reason
        # icons can be replaced with icons from theme
        # https://stackoverflow.com/questions/11643221/are-there-default-icons-in-pyqt-pyside
        # https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
        # QIcon.fromTheme("edit-clear")

        self.openFileButton = \
            create_tool_panel_button(
                "OPEN FILE",
                QIcon("resources/icons/open_file.png"))
        self.openFileButton\
            .clicked\
            .connect(self.handleOpenFileButtonClicked)

        self.openFolderButton = \
            create_tool_panel_button(
                "OPEN FOLDER",
                QIcon("resources/icons/select_folder.png"))
        self.openFolderButton\
            .clicked\
            .connect(self.handleOpenFolderButtonClicked)

        self.clearFilterRegexButton = \
            create_tool_panel_button(
                "CLEAR FILTERS",
                QIcon("resources/icons/clear.png"))
        self.clearFilterRegexButton\
            .clicked\
            .connect(self.handleClearFilterRegexButtonClicked)

        self.refreshButton = \
            create_tool_panel_button(
                "REFRESH",
                QIcon("resources/icons/refresh.png"))
        self.refreshButton\
            .clicked\
            .connect(self.handleRefreshButtonClicked)

        self.editFavouritesButton = \
            create_tool_panel_button(
                "FAVOURITES",
                QIcon("resources/icons/save_folder.png"))
        self.editFavouritesButton\
            .clicked\
            .connect(self.handleEditFavouritesButtonButtonClicked)

    def _layout_tool_panel(self):
        checkBoxesInnerLayout = QGridLayout()
        checkBoxesInnerLayout.addWidget(self.levelFilterCheckBoxes)
        checkBoxesInnerLayout.setContentsMargins(QMargins(0, 0, 0, 0))

        checkBoxesGroup = create_group_box("SHOW/HIDE")
        checkBoxesGroup.setLayout(checkBoxesInnerLayout)

        radioButtonsInnerLayout = QGridLayout()
        radioButtonsInnerLayout.addWidget(self.levelFilterRadioButtons)
        radioButtonsInnerLayout.setContentsMargins(QMargins(0, 0, 0, 0))

        radioButtonsGroup = create_group_box("SELECTION")
        radioButtonsGroup.setLayout(radioButtonsInnerLayout)

        toolPanelLayout = QHBoxLayout()
        toolPanelLayout.addWidget(self.openFileButton)
        toolPanelLayout.addWidget(self.openFolderButton)
        toolPanelLayout.addWidget(self.clearFilterRegexButton)
        toolPanelLayout.addWidget(self.refreshButton)
        toolPanelLayout.addWidget(self.editFavouritesButton)
        toolPanelLayout.addStretch(1)
        toolPanelLayout.addWidget(radioButtonsGroup)
        toolPanelLayout.addWidget(checkBoxesGroup)

        toolPanelWidget = QWidget()
        toolPanelWidget.setLayout(toolPanelLayout)
        toolPanelWidget.setFixedHeight(80)

        leftPanelSplitter = QSplitter()
        leftPanelSplitter.setOrientation(Qt.Horizontal)
        leftPanelSplitter.addWidget(self.logFilesSelector)
        leftPanelSplitter.addWidget(self._logTableMessageExceptionSplitter)

        rootLayout = QVBoxLayout()
        rootLayout.addWidget(toolPanelWidget)
        rootLayout.addWidget(leftPanelSplitter)

        self._rootWidget = QWidget()
        self._rootWidget.setLayout(rootLayout)

    def _initialize_files_list(self):
        self.logFilesSelector = LogFilesSelector()
        self.logFilesSelector\
            .fileSelectionChanged\
            .connect(self.handleFileSelectionChange)

    def load_data_from_files(self, file_paths: [str]):
        logging.info("Loading log data from files: "+", ".join(file_paths))
        data = MainWindow._load_from_files(file_paths)
        self.model.replace_log_gata(data)
        self.repaint()

    def _updateMessageErrorViewers(self, selection: QItemSelection):
        if selection.isEmpty():
            self.messageViewer.setPlainText("")
            self.exceptionViewer.setPlainText("")
        else:
            # If multiple rows are selected,
            # last row's message and error are shown
            row_idx = selection.last().bottom()

            self._setMessageAndErrorFromRow(row_idx)

    def _setMessageAndErrorFromRow(self, row_idx: int):
        index = self.sortFilterProxyModel.index(row_idx, 0)
        mapped_idx = self.sortFilterProxyModel.mapToSource(index)
        mapped_row_idx = mapped_idx.row()

        message = self.model.getMessage(mapped_row_idx)
        self.messageViewer.setPlainText(message)

        throwable = self.model.getThrowable(mapped_row_idx)
        self.exceptionViewer.setPlainText(throwable)

    #----------------------------------------------------------------------
    # Aux methods
    #----------------------------------------------------------------------
    def openFile(self, file_path):
        if file_path:
            directory_path = os.path.dirname(file_path)
            self.logFilesSelector.setMultiselectionEnabled(False)
            self.logFilesSelector.openFolder(directory_path)
            self.logFilesSelector.setSelectedFiles([file_path])

    def openFolder(self, directory_path):
        if directory_path:
            self.logFilesSelector.openFolder(directory_path)

    def reloadData(self):
        selected_file_paths = \
            self.logFilesSelector\
                .selectedFiles()
        self.load_data_from_files(selected_file_paths)

    #----------------------------------------------------------------------
    # Event (Signal) handlers
    #----------------------------------------------------------------------
    def handleLogTableSelectionChanged(self, selected: QItemSelection, deselected: QItemSelection):
        self._updateMessageErrorViewers(selected)

    def handleFilterExpressionChanged(self):
        self.sortFilterProxyModel\
            .setRegexFilters(
                {i:self.filterHeader.filterText(i) \
                 for i in range(self.filterHeader.count())})

    def handleFileSelectionChange(self):
        self.reloadData()

    def handleLogLevelCheckBoxesFilterChanged(self):
        if not self._do_not_handle_filter_changes:
            logging.debug("MainWindow.handleLogLevelCheckBoxesFilterChanged()")
            self._do_not_handle_filter_changes = True
            try:
                enabled_levels = \
                    self.levelFilterCheckBoxes\
                        .getEnabledLevels()

                logging.debug("enabled_levels = %s", enabled_levels)

                self.levelFilterRadioButtons\
                    .setEnabledLevels(enabled_levels)

                self.sortFilterProxyModel.setAllowedValuesForColumn(
                    self.model.log_level_column_idx,
                    enabled_levels)
            finally:
                self._do_not_handle_filter_changes = False

    def handleLogLevelRadioButtonFilterChanged(self):
        if not self._do_not_handle_filter_changes:
            logging.debug("MainWindow.handleLogLevelRadioButtonFilterChanged()")
            self._do_not_handle_filter_changes = True
            try:
                enabled_levels = \
                    self.levelFilterRadioButtons\
                        .getEnabledLevels()

                logging.debug("enabled_levels = %s", enabled_levels)

                self.levelFilterCheckBoxes\
                    .setEnabledLevels(enabled_levels)

                self.sortFilterProxyModel.setAllowedValuesForColumn(
                    self.model.log_level_column_idx,
                    enabled_levels)
            finally:
                self._do_not_handle_filter_changes = False

    def handleClearFilterRegexButtonClicked(self, checked):
        logging.debug("MainWindow.handleClearFilterRegexButtonClicked()")
        self.filterHeader.clearFilters()

    def handleOpenFileButtonClicked(self, checked):
        logging.debug("MainWindow.handleOpenFileButtonClicked()")
        file_path, _ = QFileDialog.getOpenFileName(self)
        self.openFile(file_path)

    def handleOpenFolderButtonClicked(self, checked):
        logging.debug("MainWindow.handleOpenFolderButtonClicked()")
        directory_path = QFileDialog.getExistingDirectory(self)
        self.openFolder(directory_path)

    def handleRefreshButtonClicked(self, checked):
        logging.debug("MainWindow.handleRefreshButtonClicked()")
        self.reloadData()

    def handleEditFavouritesButtonButtonClicked(self, clicked):
        # https://stackoverflow.com/questions/24697347/how-to-create-a-modal-window-in-pyqt
        logging.debug("MainWindow.handleEditFavouritesButtonButtonClicked()")
        
        favourite_directories_copy = \
            copy.deepcopy(self.__config.get_favourites())

        favourites_list_editor_dialog = \
            FavouriteDirectoriesListEditor(\
                favourite_directories_copy, \
                parent=self)
        favourites_list_editor_dialog.exec_()
        if favourites_list_editor_dialog.result() == QDialog.Accepted:
            logging.debug("Updating favourite directories list")            
            self.logFilesSelector\
                .setFavouriteDirectories(\
                    favourite_directories_copy)
            
            self.__config.set_favourites(favourite_directories_copy)
            self.__config.write_config()

    #----------------------------------------------------------------------
    # Drag-and-drop support
    #----------------------------------------------------------------------
    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
            # Workaround for OSx dragging and dropping
            for url in e.mimeData().urls():
                if op_sys == 'Darwin':
                    fname = str(NSURL.URLWithString_(str(url.toString())).filePathURL().path())
                else:
                    fname = str(url.toLocalFile())

            logging.debug(
                "File or folder drag-and-drooped: '%s'",
                fname)

            if os.path.isdir(fname):
                self.openFolder(fname)
            elif os.path.isfile(fname):
                self.openFile(fname)
            else:
                logging.error(
                    "Drag-and-dropped uri is not a file or folder and will be ignored: %s",
                    fname)
                e.ignore()
        else:
            e.ignore()


def _setup_logging():
    if not os.path.isdir(LOG_DIR):
        os.makedirs(LOG_DIR)
    file_handler = logging.handlers.RotatingFileHandler(LOG_PATH, mode='w', backupCount=5)
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(levelname)-5.5s]  %(message)s",
        handlers=[
            file_handler,
            logging.StreamHandler()
        ])

_setup_logging()

app=QtWidgets.QApplication(sys.argv)
window=MainWindow()
window.show()
app.exec_()