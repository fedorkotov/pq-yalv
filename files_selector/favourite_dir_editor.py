import typing
import logging

from collections import OrderedDict

from PySide2.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget, \
                              QComboBox, QToolButton, QListWidget, \
                              QMessageBox, QDialog, QTableView, QHeaderView,\
                              QLineEdit, QLabel, QPushButton, QDialog, QFileDialog
from PySide2.QtGui import QIcon
from PySide2.QtCore import Signal, QMargins, QRect, QItemSelectionModel

from common import create_tool_panel_button
from .favourite_directory import FavouriteDirectory
from .favourites_table_model import FavouritesTableModel

LABEL_WIDTH = 100
PATH_TEXTBOX_MIN_WIDTH = 300
OK_CANCEL_BTN_WIDTH = 60
OK_CANCEL_BTN_HEIGHT = 40
SMALL_BTN_WIDTH = 30

class FavouriteDirectoryEditor(QDialog):
    def __init__(
        self,\
        favourite_directory: FavouriteDirectory,
        parent:typing.Optional[QWidget] = None):

        super().__init__(parent=parent)

        self._favourite_directory = favourite_directory

        self.setModal(True)

        self.setWindowTitle('Favourite directory')

        toolPanelWidget = self._init_tool_panel_widget()

        nameTextLabel = QLabel("Name:")
        nameTextLabel.setFixedWidth(LABEL_WIDTH)
        self.nameTextEdit = QLineEdit()

        pathTextLabel = QLabel("Path:")
        pathTextLabel.setFixedWidth(LABEL_WIDTH)
        self.pathTextEdit = QLineEdit()
        self.pathTextEdit.setMinimumWidth(PATH_TEXTBOX_MIN_WIDTH)
        self.selectPathBtn = QPushButton("..")
        self.selectPathBtn.setFixedWidth(SMALL_BTN_WIDTH)
        self.selectPathBtn\
            .clicked\
            .connect(self.handleChangePathClicked)

        hboxLayoutName =  QHBoxLayout()
        hboxLayoutName.addWidget(nameTextLabel)
        hboxLayoutName.addWidget(self.nameTextEdit)

        hboxLayoutPath =  QHBoxLayout()
        hboxLayoutPath.addWidget(pathTextLabel)
        hboxLayoutPath.addWidget(self.pathTextEdit)
        hboxLayoutPath.addWidget(self.selectPathBtn)

        rootLayout = QVBoxLayout()
        rootLayout.addItem(hboxLayoutName)
        rootLayout.addItem(hboxLayoutPath)
        rootLayout.addWidget(toolPanelWidget)

        self.setLayout(rootLayout)

        self._set_data(self._favourite_directory)

    def _set_data(self, favourite_directory: FavouriteDirectory):
        self.nameTextEdit.setText(\
            favourite_directory.name)

        self.pathTextEdit.setText(\
            favourite_directory.path)


    def _init_tool_panel_widget(self):
        self._init_tool_panel_buttons()

        toolPanelLayout = QHBoxLayout()
        toolPanelLayout.addStretch(1)
        toolPanelLayout.addWidget(self.okButton)
        toolPanelLayout.addWidget(self.cancelButton)
        toolPanelLayout.addStretch(1)

        toolPanelWidget = QWidget()
        toolPanelWidget.setLayout(toolPanelLayout)
        toolPanelWidget.setFixedHeight(60)

        return toolPanelWidget


    def _init_tool_panel_buttons(self):

        self.okButton = QPushButton("Ok")
        self.okButton.setFixedWidth(OK_CANCEL_BTN_WIDTH)
        self.okButton.setMinimumHeight(OK_CANCEL_BTN_HEIGHT)
        self.okButton\
            .clicked\
            .connect(self.handleOkClicked)

        self.cancelButton = QPushButton("Cancel")
        self.cancelButton.setFixedWidth(OK_CANCEL_BTN_WIDTH)
        self.cancelButton.setMinimumHeight(OK_CANCEL_BTN_HEIGHT)
        self.cancelButton\
            .clicked\
            .connect(self.handleCancelClicked)

    def _show_error(self, message, header):
        mb = QMessageBox()
        mb.setIcon(QMessageBox.Warning)
        mb.setText(message)
        mb.setWindowTitle(header)
        mb.exec_()

    def handleOkClicked(self, checked):
        logging.debug("FavouriteDirectoryEditor.handleSaveClicked()")
        new_name = self.nameTextEdit.text()
        if not new_name:
            logging.error("Favourite directory name is empty")
            self._show_error(
                "Favourite directory name\ncan not be empty",
                "Error")
            return

        new_path = self.pathTextEdit.text()
        if not new_path:
            logging.error("Favourite directory path is empty")
            self._show_error(
                "Favourite directory name\ncan not be empty",
                "Error")
            return

        self._favourite_directory.name = self.nameTextEdit.text()
        self._favourite_directory.path = self.pathTextEdit.text()
        self.done(QDialog.Accepted)

    def handleCancelClicked(self, checked):
        logging.debug("FavouriteDirectoryEditor.handleAddClicked()")
        self.done(QDialog.Rejected)

    def handleChangePathClicked(self, checked):
        logging.debug("FavouriteDirectoriesListEditor.handleChangePathClicked()")
        directory_path = QFileDialog.getExistingDirectory(self)
        if directory_path:
            self.pathTextEdit.setText(\
                directory_path)

