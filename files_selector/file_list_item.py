import typing
import os
from collections import OrderedDict

from PySide2.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget, QRadioButton, \
                              QButtonGroup, QAbstractButton, QComboBox,\
                              QToolButton, QListWidget, QListWidgetItem, QMessageBox
from PySide2.QtGui import QColor, QIcon
from PySide2.QtCore import Qt, Signal, QMargins

from .favourite_directory import FavouriteDirectory

class FileListItem:
    def __init__(
        self,
        list_widget: QListWidget,
        folder: str,
        name: str,
        show_checkbox: bool = False):

        self.__name = name
        self.__path = os.path.join(folder, name)
        self.__isdir = os.path.isdir(self.__path)
        self.__list_item = self.__create_list_item(list_widget, show_checkbox)

    def __create_list_item(
        self,
        list_widget: QListWidget,
        show_checkbox) -> QListWidgetItem:

        if self.__isdir:
            item = QListWidgetItem(self.__name, list_widget)
            item.setIcon(QIcon.fromTheme('folder'))
            return item
        else:
            item = QListWidgetItem(self.__name, list_widget)
            item.setIcon(QIcon.fromTheme('x-office-document'))
            if show_checkbox:
                item.setCheckState(Qt.Unchecked)
                item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            else:
                item.setFlags(item.flags() & ~Qt.ItemIsUserCheckable)
            return item

    def setCheckboxEnabled(self, show_checkbox: bool):
        if not self.__isdir:
            flags = self.__list_item.flags()
            checkbox_visible = (int(flags) & int(Qt.ItemIsUserCheckable)) > 0
            if checkbox_visible != show_checkbox:
                if show_checkbox:
                    self.__list_item.setCheckState(Qt.Unchecked)
                    self.__list_item\
                        .setFlags(flags | Qt.ItemIsUserCheckable)
                else:
                    self.__list_item\
                        .setFlags(flags & ~Qt.ItemIsUserCheckable)
                    # https://stackoverflow.com/a/21618824/774130
                    # Setting Qt.ItemIsUserCheckable flag to 0 is not
                    # enough to hide checkboxes. CheckStateRole data
                    # has to be removed
                    self.__list_item.setData(Qt.CheckStateRole, None)
    @property
    def checked(self) -> bool:
        return self.__list_item.checkState() == Qt.Checked

    @checked.setter
    def checked(self, x: bool):
        if x != self.checked:
            self.__list_item\
                .setCheckState(
                    Qt.Checked if x else Qt.Unchecked)

    @property
    def list_widget_item(self) -> QListWidgetItem:
        return self.__list_item

    @property
    def name(self) -> str:
        return self.__name

    @property
    def path(self) -> str:
        return self.__path

    @property
    def isdir(self) -> bool:
        return self.__isdir