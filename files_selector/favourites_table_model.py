import typing, logging

from PySide2 import QtCore, QtGui
from PySide2.QtCore import Qt, QModelIndex
import pandas as pd

from .favourite_directory import FavouriteDirectory

# Based on
# https://www.learnpyqt.com/courses/model-views/qtableview-modelviews-numpy-pandas/


class FavouritesTableModel(QtCore.QAbstractTableModel):
    def __init__(self):
        super(FavouritesTableModel, self).__init__()

        self.replace_gata([])

    def replace_gata(self, favourite_dirs: typing.List[FavouriteDirectory]):
        self.beginResetModel()
        self._favourite_dirs = favourite_dirs
        self.endResetModel()

    def reload(self):
        # TODO: find out what is the proper way to relaod data.
        # Probably this is not the way you are supposed to
        # reload data but did not find the 'proper' way
        # and this works fine in my case
        self.beginResetModel()
        self.endResetModel()

    def data(self, index, role):
        if role == Qt.DisplayRole:
            idx_column = index.column()
            idx_row = index.row()
            if idx_column == 0:
                return self._favourite_dirs[idx_row].name
            elif idx_column == 1:
                return self._favourite_dirs[idx_row].path

    def rowCount(self, index):
        return len(self._favourite_dirs)

    def columnCount(self, index):
        return 2

    def headerData(self, section, orientation, role):
        # section is the index of the row.
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section == 0:
                    return "Name"
                else:
                    return "Path"