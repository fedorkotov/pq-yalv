import typing
import logging
import os
import sys
from collections import OrderedDict

from PySide2.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget, \
                              QComboBox, QToolButton, QListWidget, \
                              QMessageBox, QDialog, QTableView, QHeaderView
from PySide2.QtGui import QIcon
from PySide2.QtCore import Signal, QMargins, QRect, QItemSelectionModel

from common import create_tool_panel_button
from .favourite_directory import FavouriteDirectory
from .favourites_table_model import FavouritesTableModel
from .favourite_dir_editor import FavouriteDirectoryEditor

class FavouriteDirectoriesListEditor(QDialog):
    def __init__(
        self,\
        favourite_dirs: typing.List[FavouriteDirectory],
        parent:typing.Optional[QWidget] = None):

        super().__init__(parent=parent)

        self._favourite_dirs = favourite_dirs

        self.setModal(True)
        self.setWindowTitle('Favourite directories list')

        toolPanelWidget = self._init_tool_panel_widget()
        
        self._favourites_table_model = FavouritesTableModel()
        self._favourites_table_model\
            .replace_gata(self._favourite_dirs)

        self.favouritesTableWidget = QTableView()
        self.favouritesTableWidget.setModel(self._favourites_table_model)
        header = self.favouritesTableWidget.horizontalHeader()
        # Auto stretching last column of tableview to fill all available width
        # https://stackoverflow.com/a/52665000
        header.setSectionResizeMode(QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.Stretch)

        rootLayout = QVBoxLayout()
        rootLayout.addWidget(toolPanelWidget)
        rootLayout.addWidget(self.favouritesTableWidget)

        self.setLayout(rootLayout)

    def _init_tool_panel_widget(self):
        self._init_tool_panel_buttons()

        toolPanelLayout = QHBoxLayout()
        toolPanelLayout.addWidget(self.saveButton)
        toolPanelLayout.addWidget(self.editButton)
        toolPanelLayout.addWidget(self.addButton)
        toolPanelLayout.addWidget(self.moveUpButton)
        toolPanelLayout.addWidget(self.moveDownButton)
        toolPanelLayout.addWidget(self.removeButton)

        toolPanelLayout.addStretch(1)

        toolPanelWidget = QWidget()
        toolPanelWidget.setLayout(toolPanelLayout)
        toolPanelWidget.setFixedHeight(80)

        return toolPanelWidget

    def _init_tool_panel_buttons(self):
        self.saveButton = \
            create_tool_panel_button(
                "SAVE",
                QIcon("resources/icons/save.png"))
        self.saveButton\
            .clicked\
            .connect(self.handleSaveClicked)

        self.addButton = \
            create_tool_panel_button(
                "ADD",
                QIcon("resources/icons/add.png"))
        self.addButton\
            .clicked\
            .connect(self.handleAddClicked)

        self.editButton = \
            create_tool_panel_button(
                "EDIT",
                QIcon("resources/icons/folder_search.png"))
        self.editButton\
            .clicked\
            .connect(self.handleEditClicked)

        self.moveUpButton = \
            create_tool_panel_button(
                "MOVE UP",
                QIcon("resources/icons/folder_upload.png"))
        self.moveUpButton\
            .clicked\
            .connect(self.handleMoveUpClicked)

        self.moveDownButton = \
            create_tool_panel_button(
                "MOVE DOWN",
                QIcon("resources/icons/folder_download.png"))
        self.moveDownButton\
            .clicked\
            .connect(self.handleMoveDownClicked)

        self.removeButton = \
            create_tool_panel_button(
                "REMOVE",
                QIcon("resources/icons/remove.png"))
        self.removeButton\
            .clicked\
            .connect(self.handleRemoveClicked)

    def _get_selected_row_idx(self):
        return next(
            (idx.row() for idx in self.favouritesTableWidget.selectedIndexes()),
            -1)

    def _set_selected_row_idx(self, idx: int):
        self.favouritesTableWidget\
            .selectRow(idx)

    def _move_favourite_dir(self, idx: int, target_idx: int):
        logging.debug(
            "Moving favourite dir %s -> %s",
            idx,
            target_idx)

        self._favourite_dirs.insert(
            target_idx,
            self._favourite_dirs.pop(idx))

        self._favourites_table_model\
            .reload()

        self._set_selected_row_idx(target_idx)

    def _add_favourite_dir(self, favourite_dir: FavouriteDirectory):
        logging.info(
            "Adding new favourite directory '%s': '%s'",
            favourite_dir.name,
            favourite_dir.path)

        self._favourite_dirs\
            .append(favourite_dir)

        self._favourites_table_model\
            .reload()

        self._set_selected_row_idx(\
            len(self._favourite_dirs)-1)
        

    def handleSaveClicked(self, checked):
        logging.debug("FavouriteDirectoriesListEditor.handleSaveClicked()")
        self.done(QDialog.Accepted)

    def handleAddClicked(self, checked):
        logging.debug("FavouriteDirectoriesListEditor.handleAddClicked()")
        new_favourite = FavouriteDirectory("<Unknown>", "")
        favourite_dir_dialog = \
            FavouriteDirectoryEditor(\
                new_favourite,
                parent=self)
        favourite_dir_dialog.exec_()
        if favourite_dir_dialog.result() == QDialog.Accepted:            
            self._add_favourite_dir(new_favourite)            
        else:
            logging.debug("Adding new favourite directory cancelled")


    def handleMoveUpClicked(self, checked):
        logging.debug("FavouriteDirectoriesListEditor.handleMoveUpClicked()")
        selected_idx = self._get_selected_row_idx()
        if selected_idx <= 0:
            logging.debug("Can't move down the first element or list is empty")
        else:
            self._move_favourite_dir(
                selected_idx,
                selected_idx-1)

    def handleMoveDownClicked(self, checked):
        logging.debug("FavouriteDirectoriesListEditor.moveDownButton()")
        selected_idx = self._get_selected_row_idx()
        n_items = len(self._favourite_dirs)

        if selected_idx >= (n_items-1):
            logging.debug("Can't move down the last element")
        else:
            self._move_favourite_dir(
                selected_idx,
                selected_idx+1)

    def handleRemoveClicked(self, checked):
        logging.debug("FavouriteDirectoriesListEditor.handleRemoveClicked()")
        selected_idx = self._get_selected_row_idx()
        if selected_idx>=0:
            del self._favourite_dirs[selected_idx]
            self._favourites_table_model\
            .reload()

    def handleEditClicked(self, checked):
        logging.debug("FavouriteDirectoriesListEditor.handleEditClicked()")
        selected_idx = self._get_selected_row_idx()
        if selected_idx>=0:
            favourite_dir_dialog = \
                FavouriteDirectoryEditor(\
                    self._favourite_dirs[selected_idx],
                    parent=self)
            favourite_dir_dialog.exec_()
