import typing
import logging
import os
from collections import OrderedDict

from PySide2.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget, \
                              QComboBox, QToolButton, QListWidget, \
                              QMessageBox
from PySide2.QtGui import QIcon
from PySide2.QtCore import Signal, QMargins, QRect, QItemSelectionModel

from common import sorted_alphanumerically

from .favourite_directory import FavouriteDirectory
from .file_list_item import FileListItem

class LogFilesSelector(QWidget):

    fileSelectionChanged = Signal()

    BUTTON_SIZE = 25

    def __init__(
        self,\
        parent:typing.Optional[QWidget] = None):

        super().__init__(parent=parent)

        self._opened_folder = None
        self._ignore_selection_changes = False
        self._ignore_favourite_dir_changes = False
        self._multiselect_enabled = False
        self._current_folder_contents: 'List[FileListItem]' = []

        self._selected_files: 'OrderedSet[FileListItem]' = set()
        self._favourite_directories_by_id: 'OrderedDict[int, FavouriteDirectory]' = OrderedDict()
        self._favourite_directory_ids_by_path: 'Dict[str, int]' = {}
        
        self._favouriteDirectoriesComboBox = QComboBox()

        self._initMultiselectionButton()

        self._filesList = QListWidget()
        self._filesList\
            .itemSelectionChanged\
            .connect(self.handleListSelectionChange)

        self._filesList\
            .itemChanged\
            .connect(self.handleListItemChanged)

        hboxLayout = QHBoxLayout()
        hboxLayout.setContentsMargins(QMargins(0, 0, 0, 0))
        hboxLayout.addWidget(
            self._favouriteDirectoriesComboBox)
        hboxLayout.addWidget(
            self._multiSelectionCheckButton)

        favouritesWidget = QWidget()
        favouritesWidget.setLayout(hboxLayout)

        rootVboxLayout = QVBoxLayout()
        rootVboxLayout.setContentsMargins(QMargins(0, 0, 0, 0))
        rootVboxLayout.addWidget(favouritesWidget)
        rootVboxLayout.addWidget(self._filesList)

        self.setLayout(rootVboxLayout)

        self._favouriteDirectoriesComboBox\
            .currentIndexChanged\
            .connect(self.favouriteDirChanged)

    def _initMultiselectionButton(self):
        self._multiSelectionCheckButton = QToolButton()

        self._multiSelectionCheckButton\
            .setFixedSize(
                LogFilesSelector.BUTTON_SIZE,
                LogFilesSelector.BUTTON_SIZE)

        self._multiSelectionCheckButton\
            .setIcon(QIcon("resources/icons/selection.png"))
        self._multiSelectionCheckButton\
            .setCheckable(True)

        self._updateMultiselectButtonState()

        self._multiSelectionCheckButton\
            .clicked\
            .connect(self.handleMultiselectChanged)

    def _updateMultiselectButtonState(self):
        self._multiSelectionCheckButton\
            .setChecked(self._multiselect_enabled)

    def setMultiselectionEnabled(self, multiselection_enabled: bool):
        if not self._ignore_selection_changes and \
           multiselection_enabled != self._multiselect_enabled:

            logging.debug(
                'Changing multiselect %s -> %s',
                self._multiselect_enabled,
                not self._multiselect_enabled)

            self._multiselect_enabled = multiselection_enabled

            self._updateMultiselectButtonState()
            self._selected_files.clear()
            for file_list_item in self._current_folder_contents:
                file_list_item.setCheckboxEnabled(self._multiselect_enabled)

            if self._multiselect_enabled:
                self._emitFileSelectionChagned()
            else:
                self.handleListSelectionChange()


    def handleMultiselectChanged(self):
        self.setMultiselectionEnabled(not self._multiselect_enabled)

    def handleListSelectionChange(self):
        if not self._ignore_selection_changes and\
           not self._multiselect_enabled:

            n_items = len(self._current_folder_contents)
            selected_item = \
                next((self._current_folder_contents[idx.row()] \
                for idx in self._filesList.selectedIndexes() \
                if idx.row() < n_items),
                None)

            old_selected_item = \
                next(
                    self._selected_files.__iter__(),
                    None)

            if selected_item != old_selected_item:
                self._selected_files.clear()
                if selected_item:
                    self._selected_files.add(selected_item)
                self._emitFileSelectionChagned()

    def handleListItemChanged(self, item):
        if self._multiselect_enabled:
            idx = self._filesList.indexFromItem(item).row()
            if(idx < len(self._current_folder_contents)):
                file_info = self._current_folder_contents[idx]
                if file_info.checked:
                    if not file_info in self._selected_files:
                        self._selected_files.add(file_info)
                        self._emitFileSelectionChagned()
                else:
                    if file_info in self._selected_files:
                        self._selected_files.discard(file_info)
                        self._emitFileSelectionChagned()

    def _emitFileSelectionChagned(self):
        self.fileSelectionChanged.emit()
        logging.debug(\
                "Selected files list changed: %s",
                ", ".join([f.name for f in self._selected_files]))

    def setSelectedFiles(self, file_paths :'List[str]'):
        """
        Sets log file selection. Ignores files that are not in list now.
        Converts paths to absolute before comparison.
        Ignores all but first file if multiselect is turned off.
        """
        if self._multiselect_enabled:
            full_selected_paths = set([os.path.abspath(x) for x in file_paths])

            self._ignore_selection_changes = True
            try:
                self._selected_files.clear()
                for f in self._current_folder_contents:
                    if os.path.abspath(f.path) in full_selected_paths:
                        self._selected_files.add(f)
                        f.checked = True
                    else:
                        f.checked = False
            finally:
                self._ignore_selection_changes = False
        else:
            file_path = next(file_paths.__iter__(), None)
            if file_path:
                full_file_path = os.path.abspath(file_path)
                self._selected_files.clear()
                self._ignore_selection_changes = True
                try:
                    for f in self._current_folder_contents:
                        if os.path.abspath(f.path) == full_file_path:
                            self._selected_files.add(f)
                            idx = self._filesList.indexFromItem(f.list_widget_item)
                            self._filesList.setCurrentRow(idx.row())
                            break
                finally:
                    self._ignore_selection_changes = False

        self._emitFileSelectionChagned()

    def selectedFiles(self) -> 'List[str]':
        return [x.path for x in self._selected_files]

    def favouriteDirChanged(self, idx: int):
        internal_idx = self._favouriteDirectoriesComboBox\
                           .itemData(idx)
        if (internal_idx is None) or (internal_idx < 0):
            logging.debug("Favourite directory not selected")
            self._clearFileList()
            return

        favourite_dir = self._favourite_directories_by_id[internal_idx]
        logging.info(
            "Favourite directory selected: name='%s', path='%s'",
            favourite_dir.name,
            favourite_dir.path)

        if not os.path.isdir(favourite_dir.path):
            logging.error(
                "Directory '%s' does not exist",
                favourite_dir.path)

            mb = QMessageBox()
            mb.setIcon(QMessageBox.Warning)
            mb.setText(
                "Directory '{}'\ndoes not exist"\
                .format(favourite_dir.path))
            mb.setWindowTitle("Can't open directory")
            mb.exec_()
        else:
            self.openFolder(favourite_dir.path)

    def setFavouriteDirectories(
        self,\
        favourite_dirs: typing.List[FavouriteDirectory]):

        self._favourite_directories_by_id.clear()
        self._favourite_directory_ids_by_path.clear()
        for i, favourite_dir in enumerate(favourite_dirs):
            self._favourite_directories_by_id[i] = favourite_dir
            abs_path = os.path.abspath(favourite_dir.path)
            # if multiple favourites with same path exist
            # uses the first of them
            if not abs_path in self._favourite_directory_ids_by_path:
                self._favourite_directory_ids_by_path[abs_path] = i+1

        self._ignore_favourite_dir_changes = True
        try:
            self._favouriteDirectoriesComboBox.clear()
            # combo box does not support "not selected" state
            # so "not selected@ item has to be added manually
            self._favouriteDirectoriesComboBox\
                .addItem("<not selected>", userData=-1)
            for i, favourite_dir in self._favourite_directories_by_id.items():
                self._favouriteDirectoriesComboBox\
                    .addItem(favourite_dir.name, userData=i)
        finally:
            self._ignore_favourite_dir_changes = False

    def _clearFileList(self):
        self._filesList.clear()
        self._selected_files.clear()
        self._current_folder_contents.clear()

    def openFolder(self, folder_path: str):
        logging.debug("LogFilesSelector.openFolder('%s')", folder_path)
        if not os.path.isdir(folder_path):
            logging.error("'%s' is not a folder", folder_path)
            return

        logging.info("Opening directory '%s'", folder_path)

        self._clearFileList()

        # Original YALV does not do this
        # but I think alphanumeric sorting of file names
        # (aka natural sort order) will be much more convenient
        # in most cases
        files_list = \
            sorted_alphanumerically(
                os.listdir(folder_path))

        for item_name in files_list:
            item_path = os.path.join(folder_path, item_name)
            # TODO: Original YALV does not show
            # subdirectories in directory listing.
            # Maybe this should be changed.
            if os.path.isfile(item_path):                
                file_list_item = \
                    FileListItem(
                        self._filesList,
                        folder_path,
                        item_name,
                        show_checkbox=self._multiselect_enabled)
                self._current_folder_contents.append(file_list_item)
                self._filesList.addItem(file_list_item.list_widget_item)

        abs_path = os.path.abspath(folder_path)        
        favourite_idx = self._favourite_directory_ids_by_path.get(abs_path, -1)
        if favourite_idx<0:
            logging.debug("Coresponding favourite directory not found")
            
        else:
            logging.debug("Selecting folder %d", favourite_idx)
