from PySide2.QtWidgets import QWidget, QGridLayout, QFrame
from PySide2.QtGui import QPalette, QColor
from PySide2.QtCore import QMargins

class ColorFrame(QFrame):
    def __init__(self, control: QWidget, color: QColor, frame_width: int):
        super().__init__()

        vlayout = QGridLayout()
        vlayout.addWidget(control)
        vlayout.setContentsMargins(\
            QMargins(frame_width,
                     frame_width,
                     frame_width,
                     frame_width))

        palette = QPalette()
        palette.setColor(QPalette.Background, color)

        # https://srinikom.github.io/pyside-docs/PySide/QtGui/QFrame.html

        self.setContentsMargins(\
            QMargins(0, 0, 0, 0))
        self.setFrameStyle(QFrame.StyledPanel | QFrame.Plain)
        self.setAutoFillBackground(True)
        self.setPalette(palette)
        self.setLayout(vlayout)