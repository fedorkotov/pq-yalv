import typing
from PySide2.QtGui import QColor

class Log4NetLogLevel:
    def __init__(self, \
                 name: str, \
                 color: QColor, \
                 display_name : typing.Optional[str] = None):

        self._name = name
        self._color = color
        self._display_name = display_name if display_name else name

    @property
    def name(self):
        """
        Name of log level in log file
        """
        return self._name

    @property
    def color(self):
        """
        Color to use for log table
        row background color and filter control
        background color
        """
        return self._color

    @property
    def display_name(self):
        """
        Name of log level to be shown in GUI
        """
        return self._display_name
