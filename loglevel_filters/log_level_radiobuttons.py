import typing
import logging

from PySide2.QtWidgets import QHBoxLayout, QWidget, QRadioButton, \
                              QButtonGroup, QAbstractButton
from PySide2.QtGui import QColor
from PySide2.QtCore import Signal

from .log_level import Log4NetLogLevel
from .color_frame import ColorFrame

class LogLevelFilterRadioButtons(QWidget):
    """
    Horizontal array of color coded radio buttons
    (one for each log level).
    """
    selectedLevelsChanged = Signal()

    __ALL_TAG = 'all'

    def __init__(self, \
                 log_levels: typing.List[Log4NetLogLevel], \
                 parent:typing.Optional[QWidget]=None):

        super().__init__(parent=parent)

        self._log_level_names = [x.name for x in log_levels]
        self._selected_level = LogLevelFilterRadioButtons.__ALL_TAG

        self._btns_by_lvl_name = {}
        self._level_names_by_btn = {}

        self._buttonGroup = QButtonGroup()
        self._buttonGroup.setExclusive(True)

        hboxLayout = QHBoxLayout()

        # Adding invisible radiobutton to group
        # appears to be the only way to uncheck all buttons
        # https://forum.qt.io/topic/6419/how-to-uncheck-button-in-qbuttongroup/4
        self._radioButtonNone = QRadioButton()
        self._buttonGroup.addButton(self._radioButtonNone, 0)

        self._radiobuttonAll, radiobuttonAllFrame = \
            self._createRadioButton(
                LogLevelFilterRadioButtons.__ALL_TAG,
                "ALL",
                QColor('white'),
                1,
                checked=True)

        hboxLayout.addWidget(radiobuttonAllFrame)

        for idx, lvl in enumerate(log_levels):
            _, radioButtonFrame = \
                self._createRadioButton(
                    lvl.name,
                    lvl.display_name,
                    lvl.color,
                    2+idx)

            hboxLayout.addWidget(radioButtonFrame)

        self.setLayout(hboxLayout)

        self._buttonGroup\
            .buttonClicked\
            .connect(self.handleButtonClicked)

    def _createRadioButton(
        self,
        level_name: str,
        display_name: str,
        color: QColor,
        id_in_group: int,
        checked: bool = False) -> typing.Tuple[QRadioButton, QWidget]:

        radio_btn = QRadioButton(display_name)
        radio_btn.setAutoExclusive(True)
        radio_btn.setChecked(checked)

        frame_widget = \
            ColorFrame(
                radio_btn,
                color,
                1)

        self._level_names_by_btn[radio_btn] = level_name
        self._btns_by_lvl_name[level_name] = radio_btn
        self._buttonGroup.addButton(radio_btn, id_in_group)

        return radio_btn, frame_widget

    def getEnabledLevels(self):
        if self._selected_level == LogLevelFilterRadioButtons.__ALL_TAG:
            return list(self._log_level_names)
        else:
            return [self._selected_level]

    def __get_selected_level(self, selected_level_names: typing.Union[typing.List[str], typing.Set[str]]) -> str:
        if len(selected_level_names) == len(self._log_level_names):
            return LogLevelFilterRadioButtons.__ALL_TAG
        elif len(selected_level_names) == 1:
            return selected_level_names[0]
        else:
            return None

    def setEnabledLevels(self, selected_level_names: typing.Union[typing.List[str], typing.Set[str]]):
        logging.debug("LogLevelFilterRadioButtons.setEnabledLevels(%s)", selected_level_names)

        selected_level = \
            self.__get_selected_level(selected_level_names)
        logging.debug("selected_level = %s", selected_level)

        if selected_level is None:
            self._radioButtonNone.setChecked(True)
        else:
            btn = self._btns_by_lvl_name[selected_level]
            btn.setChecked(True)

    def handleButtonClicked(self, btn: QAbstractButton):
        logging.debug("LogLevelFilterRadioButtons.handleButtonClicked()")
        lvl_name = self._level_names_by_btn[btn]
        logging.debug(
            "new selected level = '%s'; old selected level = '%s'",
            lvl_name,
            self._selected_level)
        if lvl_name != self._selected_level:
            self._selected_level = lvl_name
            self.selectedLevelsChanged.emit()
