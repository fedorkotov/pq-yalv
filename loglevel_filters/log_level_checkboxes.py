import typing
from PySide2.QtWidgets import QHBoxLayout, QWidget, QCheckBox, \
                              QGridLayout, QFrame
from PySide2.QtGui import QPalette, QColor
from PySide2.QtCore import Signal, QMargins

from .log_level import Log4NetLogLevel
from .color_frame import ColorFrame

class LogLevelFilterCheckBoxes(QWidget):
    """
    Horizontal array of color coded checkboxes
    (one for each log level)
    """
    selectedLevelsChanged = Signal()

    def __init__(self, \
                 log_levels: typing.List[Log4NetLogLevel], \
                 parent:typing.Optional[QWidget]=None):

        super().__init__(parent=parent)

        self._log_level_checkboxes = {}

        hboxLayout = QHBoxLayout()

        for lvl in log_levels:
            check_box = QCheckBox("")
            check_box.setChecked(True)
            check_box.stateChanged.connect(self.handleCheckBoxCheckedChanged)

            self._log_level_checkboxes[lvl.name] = check_box

            frame_widget = \
                ColorFrame(
                    check_box,
                    lvl.color,
                    3)

            hboxLayout.addWidget(frame_widget)

        self.setLayout(hboxLayout)



    def setEnabled(self, level_name: str, enabled: typing.Optional[bool] = True):
        self._log_level_checkboxes[level_name].setChecked(enabled)

    def getEnabled(self, level_name: str) -> bool:
        return \
            self._log_level_checkboxes[level_name]\
                .isChecked()

    def getEnabledLevels(self):
        return [lvl_name for lvl_name, check_box \
                      in self._log_level_checkboxes.items() \
                      if check_box.isChecked()]

    def handleCheckBoxCheckedChanged(self, state: int):
        self.selectedLevelsChanged.emit()

    def setEnabledLevels(self, level_names: typing.Union[typing.List[str], typing.Set[str]]):
        # list is 5 items in length at most
        # no need to convert level_names to set if list is passed -
        # search in list is fast enough
        for lvl_name, check_box in self._log_level_checkboxes.items():
            check_box.setChecked(lvl_name in level_names)







