# What is it?

pq-YALV is a port of [Yet Another Log4Net Viewer](https://github.com/LukePet/YALV) by [Luca Petrini](https://github.com/LukePet)
 to python and Qt.

![Screenshot](documentation/images/screenshot-main-window.png "qt-YALV Main Window") 

Original [YALV!](https://github.com/LukePet/YALV) is a simple yet functional and convenient viewer for log4net XML logs. I strive to port as much of its functionality as possible (or at least features I use a lot)

# Why?

"Why did I write yet another "Yet Another Log4Net Viewer"? you might ask. Two reasons

1. I wanted to learn some Qt (PySide2 specifically)
2. I needed a convenient log4net/log4j log viewer for Linux and could not find one. There are a few but they are ugly and overcomplicated IMHO. I will not name them here so that nobody get offended. On Windows I use YALV! a lot and really like it. But I did not manage to get it running on Wine

