import pandas as pd
import logging
import itertools
import datetime
import xml.etree.ElementTree as ElementTree
import os

def _get_data_xpath(parameter_name):    
    return "log4j:properties/log4j:data[@name='%s']" % parameter_name

class Log4NetDataLoader:
    """
    Loads log data from one or more files to
    python lists and converts lists to pandas DataFrame
    """
    NAMESPACES = {'log4j': 'log4j'}
    TIMESTAMP_EPOCH = datetime.datetime(1970, 1, 1)

    TIMESTAMP_COLNAME = 'Timestamp'
    THREAD_COLUMN_NAME = 'Thread'
    LOGGER_COLUMN_NAME = 'Logger'
    MESSAGE_COLUMN_NAME = 'Message'
    LEVEL_COLUMN_NAME = 'LogLevel'
    THROWABLE_COLUMN_NAME = 'Throwable'
    EXECUTABLE_COLUMN_NAME = 'ExecutableName'
    USER_COLUMN_NAME = 'UserName'
    HOST_COLUMN_NAME = 'HostName'
    CLASS_COLUMN_NAME = 'Class'
    METHOD_COLUMN_NAME = 'Method'


    EXECUTABLE_XPATH = _get_data_xpath("log4japp")
    USER_XPATH = _get_data_xpath("log4net:UserName")
    HOST_XPATH = _get_data_xpath("log4net:HostName")

    def __init__(self):
        self.clear()

    @staticmethod
    def _get_xml_root(string_iterable):
        it = \
            itertools.chain(
                '<?xml version="1.0" encoding="utf-8"?><root xmlns:log4j="log4j">',
                string_iterable,
                '</root>')
        xml_root = ElementTree.fromstringlist(it)
        return xml_root

    @classmethod
    def _timestamp2datetime(cls, t):
        seconds = t//1000
        microseconds = (t-seconds*1000)*1000

        return \
            cls.TIMESTAMP_EPOCH + \
            datetime.timedelta(seconds=seconds, microseconds=microseconds)

    def clear(self):
        self._timestamps = []
        self._levels = []
        self._loggers = []
        self._messages = []
        self._threads = []
        self._throwables = []
        self._executable_names = []
        self._usernames = []
        self._hostnames = []
        self._class_names = []
        self._method_names = []

    @staticmethod
    def _get_data(logrecord, xpath):
        # xpath is passed as argument instead of
        data_node =\
            logrecord.find(
                xpath,
                namespaces=Log4NetDataLoader.NAMESPACES)
        if data_node is None:
            return None
        else:            
            return data_node.attrib['value']

    @staticmethod
    def _get_class_and_method(logrecord):
        location_info = \
            logrecord.find(
                'log4j:locationInfo',
                namespaces=Log4NetDataLoader.NAMESPACES)
        if location_info is None:
            return (None, None)
        else:
            return \
                (location_info.attrib['class'],
                 location_info.attrib['method'])

    @staticmethod
    def _get_throwable(logrecord):
        throwable = \
            logrecord.find(
                'log4j:throwable',
                namespaces=Log4NetDataLoader.NAMESPACES)

        if throwable is None:
            return None
        else:
            return throwable.text

    def append_stream(self, lines_stream):
        xml_root = Log4NetDataLoader._get_xml_root(lines_stream)
        all_logrecords = \
            xml_root.\
            findall(
                'log4j:event',
                namespaces=Log4NetDataLoader.NAMESPACES)

        for logrecord in all_logrecords:
            self._timestamps\
                .append(
                    Log4NetDataLoader._timestamp2datetime(
                        int(logrecord.attrib['timestamp'])))

            self._messages\
                .append(\
                    logrecord.find(
                        'log4j:message',
                        namespaces=Log4NetDataLoader.NAMESPACES).text)

            self._loggers\
                .append(logrecord.attrib['logger'])

            self._levels\
                .append(logrecord.attrib['level'])

            self._threads\
                .append(int(logrecord.attrib['thread']))

            self._throwables.append(
                Log4NetDataLoader._get_throwable(logrecord))

            class_name, method_name = \
                Log4NetDataLoader._get_class_and_method(logrecord)
            self._class_names.append(class_name)
            self._method_names.append(method_name)

            self._hostnames.append(
                self._get_data(
                    logrecord,
                    Log4NetDataLoader.HOST_XPATH))

            self._usernames.append(
                self._get_data(
                    logrecord,
                    Log4NetDataLoader.USER_XPATH))

            self._executable_names.append(
                self._get_data(
                    logrecord,
                    Log4NetDataLoader.EXECUTABLE_XPATH))


    def append_file(self, f_path: str):
        """
        Appends log records from specified file
        to internal collections
        """
        logging.info("Parsing xml log file '%s'", f_path)
        with open(f_path, encoding="utf-8") as f:
            self.append_stream(f)

    def append_files(self, paths):
        """
        Appends log records from specified files
        to internal lists.
        """
        for fpath in paths:
            self.append_file(fpath)

    def append_all_from_directory(self, dirpath: str):
        """
        Appends log records from all files in specified directory
        to internal lists
        """
        file_paths = []
        for fname in os.listdir(dirpath):
            fpath = os.path.join(dirpath, fname)
            if os.path.isfile(fpath):
                file_paths.append(fpath)

        self.append_files(file_paths)

    def get_dataframe(self) -> pd.DataFrame:
        """
        Returns DataFrame containing data from internal lists
        """
        logging.debug("Converting to dataframe. BEGIN")
        data = {\
            Log4NetDataLoader.TIMESTAMP_COLNAME: self._timestamps,
            Log4NetDataLoader.LEVEL_COLUMN_NAME: self._levels,
            Log4NetDataLoader.THREAD_COLUMN_NAME: self._threads,
            Log4NetDataLoader.LOGGER_COLUMN_NAME: self._loggers,
            Log4NetDataLoader.MESSAGE_COLUMN_NAME: self._messages,
            Log4NetDataLoader.THROWABLE_COLUMN_NAME: self._throwables,
            Log4NetDataLoader.USER_COLUMN_NAME: self._usernames,
            Log4NetDataLoader.HOST_COLUMN_NAME: self._hostnames,
            Log4NetDataLoader.CLASS_COLUMN_NAME: self._class_names,
            Log4NetDataLoader.METHOD_COLUMN_NAME: self._method_names,
            Log4NetDataLoader.EXECUTABLE_COLUMN_NAME: self._executable_names}

        df = pd.DataFrame(data)

        logging.debug("Converting to dataframe. END")

        return df

    @staticmethod
    def get_empty_data() -> pd.DataFrame:
        return pd.DataFrame(
            {Log4NetDataLoader.TIMESTAMP_COLNAME:[],
             Log4NetDataLoader.LEVEL_COLUMN_NAME: [],
             Log4NetDataLoader.THREAD_COLUMN_NAME: [],
             Log4NetDataLoader.LOGGER_COLUMN_NAME: [],
             Log4NetDataLoader.MESSAGE_COLUMN_NAME: [],
             Log4NetDataLoader.THROWABLE_COLUMN_NAME: [],
             Log4NetDataLoader.USER_COLUMN_NAME: [],
            Log4NetDataLoader.HOST_COLUMN_NAME: [],
            Log4NetDataLoader.CLASS_COLUMN_NAME: [],
            Log4NetDataLoader.METHOD_COLUMN_NAME: [],
            Log4NetDataLoader.EXECUTABLE_COLUMN_NAME: []})

