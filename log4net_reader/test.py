import unittest
import io

import numpy as np

from .log4net2pandas import Log4NetDataLoader

LOG4NET_LOG_EAMPLE = """<log4j:event logger="logger1" timestamp="1582621447520" level="ERROR" thread="26"><log4j:message>Обработка задания завершилась с ошибкой</log4j:message><log4j:properties><log4j:data name="log4jmachinename" value="machine1" /><log4j:data name="log4japp" value="executable.exe" /><log4j:data name="log4net:Identity" value="" /><log4j:data name="log4net:UserName" value="user1" /><log4j:data name="log4net:HostName" value="host1" /></log4j:properties><log4j:throwable><![CDATA[Multiline
Exception]]></log4j:throwable><log4j:locationInfo class="Program" method="Main" file="" line="0" /></log4j:event>
<log4j:event logger="logger2" timestamp="1582621447689" level="INFO" thread="10"><log4j:message>Success</log4j:message><log4j:properties><log4j:data name="log4jmachinename" value="machine1" /><log4j:data name="log4japp" value="executable2.exe" /><log4j:data name="log4net:Identity" value="" /><log4j:data name="log4net:UserName" value="user2" /><log4j:data name="log4net:HostName" value="host2" /></log4j:properties><log4j:locationInfo class="Class1" method="Method1" file="" line="0" /></log4j:event>"""

class TestLog4NetDataLoader(unittest.TestCase):
    def test_log_record_fields_presence(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        self.assertIn(Log4NetDataLoader.TIMESTAMP_COLNAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.THREAD_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.LOGGER_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.MESSAGE_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.LEVEL_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.THROWABLE_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.EXECUTABLE_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.USER_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.HOST_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.CLASS_COLUMN_NAME, log_data_frame.columns)
        self.assertIn(Log4NetDataLoader.METHOD_COLUMN_NAME, log_data_frame.columns)

    def test_timestamp(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        timestamps = log_data_frame[Log4NetDataLoader.TIMESTAMP_COLNAME]

        self.assertEqual(
            timestamps.values[0],
            np.datetime64('2020-02-25T09:04:07.52'))

        self.assertEqual(
            timestamps.values[1],
            np.datetime64('2020-02-25T09:04:07.689'))

    def test_loglevel(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        levels = log_data_frame[Log4NetDataLoader.LEVEL_COLUMN_NAME]

        self.assertEqual(
            levels.values[0],
            'ERROR')

        self.assertEqual(
            levels.values[1],
            'INFO')

    def test_threads(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        thread_ids = log_data_frame[Log4NetDataLoader.THREAD_COLUMN_NAME]

        self.assertEqual(
            thread_ids.values[0],
            26)

        self.assertEqual(
            thread_ids.values[1],
            10)

    def test_logger_name(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        logger_names = log_data_frame[Log4NetDataLoader.LOGGER_COLUMN_NAME]

        self.assertEqual(
            logger_names.values[0],
            'logger1')

        self.assertEqual(
            logger_names.values[1],
            'logger2')

    def test_messages(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        messages = log_data_frame[Log4NetDataLoader.MESSAGE_COLUMN_NAME]

        self.assertEqual(
            messages.values[0],
            'Обработка задания завершилась с ошибкой')

        self.assertEqual(
            messages.values[1],
            'Success')

    def test_throwables(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        messages = log_data_frame[Log4NetDataLoader.THROWABLE_COLUMN_NAME]

        self.assertEqual(
            messages.values[0],
            'Multiline\nException')

        self.assertEqual(
            messages.values[1],
            None)

    def test_executable_names(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        executable_names = log_data_frame[Log4NetDataLoader.EXECUTABLE_COLUMN_NAME]

        self.assertEqual(
            executable_names.values[0],
            'executable.exe')

        self.assertEqual(
            executable_names.values[1],
            'executable2.exe')

    def test_user_names(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        user_names = log_data_frame[Log4NetDataLoader.USER_COLUMN_NAME]

        self.assertEqual(
            user_names.values[0],
            'user1')

        self.assertEqual(
            user_names.values[1],
            'user2')

    def test_host_names(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        host_names = log_data_frame[Log4NetDataLoader.HOST_COLUMN_NAME]

        self.assertEqual(
            host_names.values[0],
            'host1')

        self.assertEqual(
            host_names.values[1],
            'host2')

    def test_class_names(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        class_names = log_data_frame[Log4NetDataLoader.CLASS_COLUMN_NAME]

        self.assertEqual(
            class_names.values[0],
            'Program')

        self.assertEqual(
            class_names.values[1],
            'Class1')

    def test_method_names(self):
        data_stream = io.StringIO(LOG4NET_LOG_EAMPLE)
        loader = Log4NetDataLoader()
        loader.append_stream(data_stream)
        log_data_frame = loader.get_dataframe()

        method_names = log_data_frame[Log4NetDataLoader.METHOD_COLUMN_NAME]

        self.assertEqual(
            method_names.values[0],
            'Main')

        self.assertEqual(
            method_names.values[1],
            'Method1')